/// Flutter code sample for Slider

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  double _valorAtual = 0;
  double _porcento = 0;
  double _limite = 1400;

  void _porcentagemValor(double max, double value){
    _porcento = (value * 100) /max;
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // slider iniciando no 0 terminando no 100 com o passo de 10 em 10
        Slider(
          value: _valorAtual,
          //min: 0.0,
          //max: 1400.0,
          min: 0,
          max: _limite,
          divisions: 10,
          label: (_porcento.toStringAsFixed(0) + "%"),
          onChanged: (value) {
            setState(() {
              _valorAtual= value;
              _porcentagemValor(_limite, _valorAtual);
            });
          },
        ),
        SizedBox(height: 30),
        Text("R\$" + _valorAtual.toStringAsFixed(2), style: TextStyle(fontSize: 30),)
        //Text("%" + _currentSliderValue.toStringAsFixed(0), style: TextStyle(fontSize: 30),)
      ],
    );
  }
}
